package com.example.kimsoerhrd.daggerretrofit.app.di.module;

import com.example.kimsoerhrd.daggerretrofit.api.ArticleApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = NetworkModule.class)

public class RestfuModule {
    private final String BASE_URL = "http://ams.chhaileng.com:8080/";

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient okHttpClient){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    public ArticleApi provideArticleApi(OkHttpClient okHttpClient){
        return provideRetrofit(okHttpClient).create(ArticleApi.class);
    }
}
