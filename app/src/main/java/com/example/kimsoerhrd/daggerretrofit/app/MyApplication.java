package com.example.kimsoerhrd.daggerretrofit.app;

import android.app.Application;

import com.example.kimsoerhrd.daggerretrofit.app.di.component.ApplicationComponent;
import com.example.kimsoerhrd.daggerretrofit.app.di.component.DaggerApplicationComponent;
import com.example.kimsoerhrd.daggerretrofit.app.di.module.RestfuModule;

public class MyApplication extends Application {
    private ApplicationComponent component;
    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.builder()
                .restfuModule(new RestfuModule())
                .build();
       // component.inject(this);

    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
