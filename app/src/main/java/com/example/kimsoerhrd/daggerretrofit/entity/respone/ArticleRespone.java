package com.example.kimsoerhrd.daggerretrofit.entity.respone;

import com.example.kimsoerhrd.daggerretrofit.entity.Article;
import com.example.kimsoerhrd.daggerretrofit.entity.Pagination;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleRespone {

    @SerializedName("PAGINATION")
    public Pagination pagination;
    @SerializedName("DATA")
    public List<Article> articles;
    @SerializedName("MESSAGE")
    public String MESSAGE;
    @SerializedName("CODE")
    public String CODE;



}
