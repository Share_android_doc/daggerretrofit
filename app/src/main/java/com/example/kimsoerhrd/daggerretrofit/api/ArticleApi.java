package com.example.kimsoerhrd.daggerretrofit.api;



import com.example.kimsoerhrd.daggerretrofit.entity.Article;
import com.example.kimsoerhrd.daggerretrofit.entity.respone.ArticleRespone;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ArticleApi {
    @GET("v1/api/articles")
    Call<ArticleRespone> findAll();
}
