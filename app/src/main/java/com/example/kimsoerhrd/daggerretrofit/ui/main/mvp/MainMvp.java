package com.example.kimsoerhrd.daggerretrofit.ui.main.mvp;

import com.example.kimsoerhrd.daggerretrofit.base.BaseMvpPresenter;
import com.example.kimsoerhrd.daggerretrofit.base.BaseMvpView;
import com.example.kimsoerhrd.daggerretrofit.callback.CallBackWithList;
import com.example.kimsoerhrd.daggerretrofit.entity.Article;

public interface MainMvp {

    interface View extends BaseMvpView {

    }

    interface Presenter extends BaseMvpPresenter<MainMvp.View> {
        void findAllArticles();
    }

    interface Interactor {
        void findAllArticles(CallBackWithList<Article> callBackWithList);
    }
}
