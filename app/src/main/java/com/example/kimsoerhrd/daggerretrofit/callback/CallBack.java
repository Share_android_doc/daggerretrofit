package com.example.kimsoerhrd.daggerretrofit.callback;

public interface CallBack {
    void onSuccess();
    void onFailed();
}
