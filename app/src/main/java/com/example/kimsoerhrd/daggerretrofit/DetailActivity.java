package com.example.kimsoerhrd.daggerretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.kimsoerhrd.daggerretrofit.api.ArticleApi;
import com.example.kimsoerhrd.daggerretrofit.app.MyApplication;
import com.google.gson.JsonObject;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    @Inject
    ArticleApi articleApi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ((MyApplication)getApplication()).getComponent().inject(this);
        
    }
}
