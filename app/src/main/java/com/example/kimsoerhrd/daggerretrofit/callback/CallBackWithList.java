package com.example.kimsoerhrd.daggerretrofit.callback;

import java.util.List;

public interface CallBackWithList<T> {
    void onSuccess(List<T> list);

    void onFailed();

    void onError();

    void onComplete();
}
