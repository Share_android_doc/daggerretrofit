package com.example.kimsoerhrd.daggerretrofit.entity;

import com.google.gson.annotations.SerializedName;

public class Category {
    @SerializedName("NAME")
    public String NAME;
    @SerializedName("ID")
    public int ID;
}
