package com.example.kimsoerhrd.daggerretrofit.entity;

import com.google.gson.annotations.SerializedName;

public class Pagination {
    @SerializedName("TOTAL_PAGES")
    public int TOTAL_PAGES;
    @SerializedName("TOTAL_COUNT")
    public int TOTAL_COUNT;
    @SerializedName("LIMIT")
    public int LIMIT;
    @SerializedName("PAGE")
    public int PAGE;
}
