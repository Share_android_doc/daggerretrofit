package com.example.kimsoerhrd.daggerretrofit.ui.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.kimsoerhrd.daggerretrofit.DetailActivity;
import com.example.kimsoerhrd.daggerretrofit.R;
import com.example.kimsoerhrd.daggerretrofit.api.ArticleApi;
import com.example.kimsoerhrd.daggerretrofit.app.MyApplication;
import com.example.kimsoerhrd.daggerretrofit.base.BaseActivity;
import com.example.kimsoerhrd.daggerretrofit.entity.respone.ArticleRespone;
import com.google.gson.JsonObject;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {


    @Inject
    ArticleApi articleApi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((MyApplication)getApplication()).getComponent().inject(this);
        articleApi.findAll().enqueue(new Callback<ArticleRespone>() {
            @Override
            public void onResponse(Call<ArticleRespone> call, Response<ArticleRespone> response) {
                Log.e("ooooo", response.body().pagination.TOTAL_COUNT+"");
            }

            @Override
            public void onFailure(Call<ArticleRespone> call, Throwable t) {

            }
        });
    }

    public void onDetail(View view) {
        startActivity(new Intent(this, DetailActivity.class));
    }
}
