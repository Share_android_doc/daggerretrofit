package com.example.kimsoerhrd.daggerretrofit.entity;

import com.google.gson.annotations.SerializedName;



public class Article {

    @SerializedName("IMAGE")
    public String image;
    @SerializedName("CATEGORY")
    public Category category;
    @SerializedName("STATUS")
    public String status;
    @SerializedName("AUTHOR")
    public Author author;
    @SerializedName("CREATED_DATE")
    public String createdDate;
    @SerializedName("DESCRIPTION")
    public String description;
    @SerializedName("TITLE")
    public String title;
    @SerializedName("ID")
    public int id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Article{" +
                "image='" + image + '\'' +
                ", category=" + category +
                ", status='" + status + '\'' +
                ", author=" + author +
                ", createdDate='" + createdDate + '\'' +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", id=" + id +
                '}';
    }
}
