package com.example.kimsoerhrd.daggerretrofit.base;

public interface BaseMvpView {
    void showLoading();
    void hideLoading();
}
