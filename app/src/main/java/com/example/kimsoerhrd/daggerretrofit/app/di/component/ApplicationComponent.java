package com.example.kimsoerhrd.daggerretrofit.app.di.component;

import com.example.kimsoerhrd.daggerretrofit.DetailActivity;
import com.example.kimsoerhrd.daggerretrofit.ui.main.MainActivity;
import com.example.kimsoerhrd.daggerretrofit.app.di.module.RestfuModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = RestfuModule.class)
public interface ApplicationComponent {

    void inject(MainActivity mainActivity);
    void inject(DetailActivity detailActivity);
}
