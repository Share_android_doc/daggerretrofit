package com.example.kimsoerhrd.daggerretrofit.base;

public interface BaseMvpPresenter<V extends BaseMvpView> {

    void onAttach(V mvpView);

    void onDetach();
}
